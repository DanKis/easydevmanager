import sys
from PyQt5.QtWidgets import QApplication, QWidget


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._run()

    def _run(self):
        self.setGeometry(100, 100, 1200, 900)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = MainWindow()
    sys.exit(app.exec_())
